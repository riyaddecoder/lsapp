@extends('layouts.app')

@section('content')
<h1>Edit post</h1>
<form action="/posts/{{$post->id}}" method="POST">
    <div>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <textarea name="body" id="body" cols="30" rows="10" class="form-control">{{$post->body}}</textarea>
        </div>
        <input type="submit" value="Submit" class="btn btn-primary">
        {{method_field('PUT')}}
    </div>
    @csrf
</form>
@endsection