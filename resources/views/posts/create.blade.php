@extends('layouts.app')

@section('content')
<h1>Create New Post</h1>
<form action="/posts" method="POST">
    <div>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" id="title" class="form-control">
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <textarea name="body" id="body" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <input type="submit" value="Submit" class="btn btn-primary">
    </div>
    @csrf
</form>
@endsection