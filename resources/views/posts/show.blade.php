@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-success mb-5">Go Back</a>
    <h1>{{$post->title}}</h1>
    <div>
        {{$post->body}}
    </div>
    <hr>
    <small>Written on {{$post->created_at}}</small>
    <hr>
    <a href="/posts/{{$post->id}}/edit" class="btn btn-dark">Edit</a>
    <form action="/posts/{{$post->id}}" method="POST" class="float-right">
        {{method_field('DELETE')}}
        <input type="submit" value="Delete" class="btn btn-danger">
        @csrf
    </form>
@endsection